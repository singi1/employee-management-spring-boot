package dev.employeemanager.singi;

import dev.employeemanager.singi.employee.Employee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@SpringBootApplication
public class EmployeeManagingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagingApiApplication.class, args);
	}

}
