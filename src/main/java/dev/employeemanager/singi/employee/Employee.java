package dev.employeemanager.singi.employee;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table
public class Employee {

    @Id
    @SequenceGenerator(
            name = "employee_sequence",
            sequenceName = "employee_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "employee_sequence"
    )
    private Integer id;
    private String name;
    private String email;
    @Transient
    private Integer age;
    private LocalDate date_of_birth;
    private String employee_id;
    private LocalDate date_started;
    private boolean is_still_employed;
    private LocalDate date_retired;
    private String role;
    private Integer pay;

    public Employee() {
    }

    public Employee(Integer id, String name, String email,
                    LocalDate date_of_birth,
                    String employee_id, LocalDate date_started,
                    boolean is_still_employed, LocalDate date_retired,
                    String role, Integer pay) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.date_of_birth = date_of_birth;
        this.employee_id = employee_id;
        this.date_started = date_started;
        this.is_still_employed = is_still_employed;
        this.date_retired = date_retired;
        this.role = role;
        this.pay = pay;
    }

    public Employee(String name, String email,
                    LocalDate date_of_birth, String employee_id,
                    LocalDate date_started, boolean is_still_employed,
                    LocalDate date_retired, String role, Integer pay) {
        this.name = name;
        this.email = email;
        this.date_of_birth = date_of_birth;
        this.employee_id = employee_id;
        this.date_started = date_started;
        this.is_still_employed = is_still_employed;
        this.date_retired = date_retired;
        this.role = role;
        this.pay = pay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return Period.between(date_of_birth, LocalDate.now()).getYears();
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public LocalDate getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDate date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public LocalDate getDate_started() {
        return date_started;
    }

    public void setDate_started(LocalDate date_started) {
        this.date_started = date_started;
    }

    public boolean isIs_still_employed() {
        return is_still_employed;
    }

    public void setIs_still_employed(boolean is_still_employed) {
        this.is_still_employed = is_still_employed;
    }

    public LocalDate getDate_retired() {
        return date_retired;
    }

    public void setDate_retired(LocalDate date_retired) {
        this.date_retired = date_retired;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getPay() {
        return pay;
    }

    public void setPay(Integer pay) {
        this.pay = pay;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", date_of_birth=" + date_of_birth +
                ", employee_id='" + employee_id + '\'' +
                ", date_started=" + date_started +
                ", is_still_employed=" + is_still_employed +
                ", date_retired=" + date_retired +
                ", role='" + role + '\'' +
                ", pay=" + pay +
                '}';
    }
}
