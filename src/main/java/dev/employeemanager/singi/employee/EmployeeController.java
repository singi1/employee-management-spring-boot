package dev.employeemanager.singi.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @PostMapping
    public void registerEmployee(@RequestBody Employee employee){
        employeeService.addNewEmployee(employee);
    }

    @DeleteMapping(path = "{employeeId}")
    public void deleteEmployee(@PathVariable("employeeId") Integer employeeId) {
        employeeService.deleteEmployee(employeeId);
    }

    @PutMapping(path = "{employeeId}")
    public void updateEmployee(
            @PathVariable("employeeId") Integer employeeId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) LocalDate date_of_birth,
            @RequestParam(required = false) String employee_id,
            @RequestParam(required = false) LocalDate date_started,
            @RequestParam(required = false) boolean is_still_employed,
            @RequestParam(required = false) LocalDate date_retired,
            @RequestParam(required = false) String role,
            @RequestParam(required = false) Integer pay){
        employeeService.updateEmployee(employeeId, name, email, date_of_birth,
                employee_id, date_started, is_still_employed, date_retired,
                role, pay);
    }

}
