package dev.employeemanager.singi.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public List<Employee> getEmployees(){
        return employeeRepository.findAll();
    }

    public void addNewEmployee(Employee employee) {
        Optional<Employee> employeeOptional = employeeRepository.findEmployeeByEmail(employee.getEmail());
        if (employeeOptional.isPresent()){
            throw new IllegalStateException("email taken");
        }
        employeeRepository.save(employee);
    }

    public void deleteEmployee(Integer employeeId) {
        boolean exists = employeeRepository.existsById(employeeId);
        if (!exists) {
            throw new IllegalStateException("employee with id " + employeeId + "does not exist");
        }
        employeeRepository.deleteById(employeeId);
    }

    @Transactional
    public void updateEmployee(Integer employeeId, String name,
                               String email, LocalDate date_of_birth,
                               String employee_id, LocalDate date_started,
                               boolean is_still_employed, LocalDate date_retired,
                               String role, Integer pay) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new IllegalStateException(
                        "Employee with id " + employeeId + " does not exist!"
                ));

        if (name != null && name.length() > 0 &&
                !Objects.equals(employee.getName(), name)) {
            employee.setName(name);
        }

        if (email != null && email.length() > 0 &&
                !Objects.equals(employee.getEmail(), email)) {
            Optional<Employee> employeeOptional = employeeRepository
                    .findEmployeeByEmail(email);
            if (employeeOptional.isPresent()){
                throw new IllegalStateException("email taken");
            }
            employee.setEmail(email);
        }

        if (date_of_birth != null &&
                !Objects.equals(employee.getDate_of_birth(), date_of_birth)) {
            employee.setDate_of_birth(date_of_birth);
        }

        if (employee_id != null && employee_id.length() > 0 &&
                !Objects.equals(employee.getEmployee_id(), employee_id)) {
            employee.setEmployee_id(employee_id);
        }

        if (date_started != null &&
                !Objects.equals(employee.getDate_started(), date_started)) {
            employee.setDate_started(date_started);
        }

        if (!Objects.equals(employee.isIs_still_employed(), is_still_employed)) {
            employee.setIs_still_employed(is_still_employed);
        }

        if (date_retired != null &&
                !Objects.equals(employee.getDate_retired(), date_retired)) {
            employee.setDate_retired(date_started);
        }

        if (role != null && role.length() > 0 &&
                !Objects.equals(employee.getRole(), role)) {
            employee.setRole(role);
        }

        if (pay != null &&
                !Objects.equals(employee.getPay(), pay)){
            employee.setPay(pay);
        }
        employeeRepository.save(employee);
    }
}
