package dev.employeemanager.singi.employee;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class EmployeeConfig {

    @Bean
    CommandLineRunner commandLineRunner(EmployeeRepository employeeRepository){
        return args -> {
            Employee Mark = new Employee(
                    "Mark",
                    "mark@outlook.com",
                    LocalDate.of(1998, Month.MARCH, 25),
                    "000001",
                    LocalDate.of(2018, Month.MAY, 11),
                    true,
                    null,
                    "DevOps Engineer",
                    1500
            );

            Employee Anna = new Employee(
                    "Anna",
                    "anna99@outlook.com",
                    LocalDate.of(1999, Month.MARCH, 1),
                    "000002",
                    LocalDate.of(2018, Month.MAY, 11),
                    true,
                    null,
                    "Accounting",
                    800
            );

            Employee Phil = new Employee(
                    "Phil",
                    "phil@outlook.com",
                    LocalDate.of(1992, Month.JUNE, 3),
                    "000003",
                    LocalDate.of(2016, Month.APRIL, 9),
                    true,
                    null,
                    "DevOps Engineer",
                    3000
            );

            employeeRepository.saveAll(
                    List.of(Mark, Anna, Phil)
            );
        };
    }
}
